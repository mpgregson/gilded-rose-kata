describe("Gilded Rose", () => {
  beforeEach(function() {
    items = [];
  });
  it("Should lower the sell_in of standard items", () => {
    items.push(new Item("Standard item", 5, 0));

    update_quality();

    expect(items[0].sell_in).toBe(4);
  });
  it("Should lower the quality of ONLY standard items", () => {
    items.push(new Item("Standard item", 5, 20));

    update_quality();

    expect(items[0].quality).toBe(19);
  });
  it("Should degrade quality twice as fast once sell_in < 0", () => {
    items.push(new Item("Standard item", -1, 10));

    update_quality();

    expect(items[0].quality).toBe(8);
  });
  it("Should not allow negative quality", () => {
    items.push(new Item("Standard item", 0, 0));

    update_quality();

    expect(items[0].quality).toBe(0);
  });
  it("Should increase the quality of aged brie", () => {
    items.push(new Item("Aged Brie", 10, 10));

    update_quality();

    expect(items[0].quality).toBe(11);
  });
  it("Should not allow quality to be > 50", () => {
    items.push(new Item("Aged Brie", 10, 50));

    update_quality();

    expect(items[0].quality).toBe(50);
  });
  it("Should not change the sell_in or quality of Sufuras", () => {
    items.push(new Item("Sulfuras, Hand of Ragnaros", 10, 10));

    update_quality();

    expect(items[0].sell_in).toBe(10);
    expect(items[0].quality).toBe(10);
  });
  it("Should handle backstage passes correctly", () => {
    items.push(new Item("Backstage passes to a TAFKAL80ETC concert", 11, 0));

    update_quality(); //11 Days

    expect(items[0].quality).toBe(1);

    update_quality(); //10 Days

    expect(items[0].quality).toBe(3);

    update_quality(); //9 Days

    expect(items[0].quality).toBe(5);

    update_quality(); //8 Days
    update_quality(); //7 Days
    update_quality(); //6 Days

    expect(items[0].quality).toBe(11);

    update_quality(); //5 Days

    expect(items[0].quality).toBe(14);

    update_quality(); //4 Days
    update_quality(); //3 Days
    update_quality(); //2 Days
    update_quality(); //1 Days
    update_quality(); //0 Days

    expect(items[0].quality).toBe(0);
  });
  it("Degrade conjured items twice as fast", () => {
    items.push(new Item("Conjured Mana Cake", 1, 10));

    update_quality();

    expect(items[0].quality).toBe(8);

    update_quality();

    expect(items[0].quality).toBe(4);
  });
});
