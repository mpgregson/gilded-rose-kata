function Item(name, sell_in, quality) {
  this.name = name;
  this.sell_in = sell_in;
  this.quality = quality;
}

var items = [];

const BRIE = "Aged Brie",
  PASSES = "Backstage passes to a TAFKAL80ETC concert",
  SULFURAS = "Sulfuras, Hand of Ragnaros",
  CONJURED = "Conjured Mana Cake";

items.push(new Item("+5 Dexterity Vest", 10, 20));
items.push(new Item("Aged Brie", 2, 0));
items.push(new Item("Elixir of the Mongoose", 5, 7));
items.push(new Item("Sulfuras, Hand of Ragnaros", 0, 80));
items.push(new Item("Backstage passes to a TAFKAL80ETC concert", 15, 20));
items.push(new Item("Conjured Mana Cake", 3, 6));

function update_quality() {
  items.map(item => {
    if (item.name == SULFURAS) {
      //Do nothing
      return;
    }

    item.handleQuality();
    item.decreaseSellIn();

    if (item.sell_in < 0) {
      //Run again to double the quality increase/decrease
      item.handleQuality();
    }
  });
}

function getIncreaseByValue(item) {
  if (item.name == BRIE || item.sell_in > 10) {
    // Brie or pass with 10 or more days
    return 1;
  } else if (item.sell_in <= 5) {
    // Pass with 5 or less days
    return 3;
  } else {
    // Pass with between 5 and 10 days
    return 2;
  }
}

function getDecreaseByValue(item) {
  // double for conjured
  return item.name == CONJURED ? 2 : 1;
}

function isExpiredPass(item) {
  return item.name == PASSES && item.sell_in < 0;
}

Item.prototype.handleQuality = function() {
  if ([BRIE, PASSES].includes(this.name)) {
    // Maturing item
    this.quality = Math.min(
      this.quality + getIncreaseByValue(this),
      isExpiredPass(this) ? 0 : 50
    );
  } else {
    // Standard item
    this.quality = Math.max(this.quality - getDecreaseByValue(this), 0);
  }
};

Item.prototype.decreaseSellIn = function() {
  this.sell_in = this.sell_in - 1;
};
